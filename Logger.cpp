#include "Logger.h"
#include <string.h>

unsigned int Logger::staticVari = 1;

Logger::Logger(char* filename, bool logToScreen)
{
	this->_outStream = new OutStream();
	this->_fileStrm = new fileStream(filename);
	this->_logToScreen = logToScreen;
	this->_filename = filename;
}

Logger::~Logger()
{
	delete(_outStream);
	delete(_fileStrm);
}

void Logger::print(const char* msg)
{
	this->_fileStrm->operator<<(this->staticVari);
	this->_fileStrm->operator<<(" ");
	this->_fileStrm->operator<<(msg);
	if(this->_logToScreen)
	{
		this->_outStream->setFile(stdout);
		this->_outStream->operator<<(this->staticVari);
		this->_outStream->operator<<(" ");
		this->_outStream->operator<<(msg);
	}
	(this->staticVari)++;
}
