#pragma once
#include "../streamlib/streamlib.h"
#pragma comment(lib, "../x64/Debug/streamlib.lib")

using namespace msl;
int main()
{
	OutStream myClass = OutStream();
	const char* strPart1 = "i'm the doctor and i'm ";
	const char* strPart2 = " years old";
	myClass.operator<<(strPart1);
	myClass.operator<<(1500);
	myClass.operator<<(strPart2);
	void (*endLiner)() = endline;
	myClass.operator<<(*endLiner);

	return 0;
}