#pragma once
#include "OutStream.h"
#include "FileStream.h"

class Logger
{
	OutStream* _outStream;
	fileStream* _fileStrm;
	bool _logToScreen;
	const char* _filename;
	static unsigned int staticVari;
public:
	Logger(char* filename, bool logToScreen);
	~Logger();

	void print(const char *msg);
};
