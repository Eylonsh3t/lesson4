#pragma once
#include "OutStream.h"

class OutStreamEncrypted : public OutStream
{
private:
	int _key;
	char* _cipher;
public:
	void setKey();
	void setMsg();
	void caesarCipher();
	void release();
	OutStream& operator<<(char* string);
	OutStream& operator<<(int number);
};