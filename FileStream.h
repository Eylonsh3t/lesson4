#pragma once
#include "OutStream.h"

class fileStream : public OutStream
{
public:
	fileStream(char* filePath) : OutStream()
	{
		this->_file = fopen(filePath, "w");
	}
	~fileStream();
};