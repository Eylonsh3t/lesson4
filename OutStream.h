#pragma once
#include <cstdio>

class OutStream
{
protected:
	FILE* _file;
public:
	OutStream();
	~OutStream();
	void setFile(FILE* file);

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
};

void endline();