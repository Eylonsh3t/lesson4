#include "OutStream.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 100
#define MAX_NUM 126

OutStream::OutStream()
{
	this->setFile(stdout);
}

OutStream::~OutStream()
{
}

void OutStream::setFile(FILE* file)
{
	this->_file = file;
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->_file, str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(this->_file, "%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}

void endline()
{
	printf("\n");
}
