#include "OutStream.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "OutStreamEncrypted.h"

#define MAX 100
#define MAX_NUM 126

void OutStreamEncrypted::setKey()
{
	int key = 0;
	printf("Enter key number: ");
	scanf("%d", &key);
	getchar();
	this->_key = key;
}

void OutStreamEncrypted::setMsg()
{
	this->_cipher = (char*)malloc(MAX);
	printf("Enter the message: ");
	fgets(this->_cipher, MAX, stdin);
	strtok(this->_cipher, "\n");
}

void OutStreamEncrypted::caesarCipher()
{
	char* decrypted = (char*)malloc(MAX);
	char chr = 0;
	int counter = 0;
	for (int i = 0; this->_cipher[i] != '\0'; ++i) {
		chr = this->_cipher[i];
		if (chr >= ' ' && chr <= '~')
		{
			int check = int(chr);
			check = check + this->_key;
			if (check > MAX_NUM)
			{
				do
				{
					check = check - MAX_NUM;
				} while (check > MAX_NUM);
				chr = ' ' + check;
			}
			else
			{
				chr = chr + this->_key;
			}
			decrypted[i] = chr;
			counter++;
		}
		else
		{
			printf("Can't decrypt this letter it has to be between 32 to 126!");
		}
	}
	for (int i = 0; i < counter; i++)
	{
		printf("%c", decrypted[i]);
	}
	free(decrypted);
}

void OutStreamEncrypted::release()
{
	free(this->_cipher);
}

OutStream& OutStreamEncrypted::operator<<(char* string)
{
	this->_cipher = string;
	this->caesarCipher();
	return *this;
}

OutStream& OutStreamEncrypted::operator<<(int number)
{
	if (number >= 0)
	{
		char chr = 0;
		if (number + this->_key > MAX_NUM)
		{
			int check = number + this->_key;
			do
			{
				check = check - MAX_NUM;
			} while (check > MAX_NUM);
			chr = ' ' + check;
		}
		else
		{
			chr = number + this->_key;
		}
		printf("%c", chr);
	}
	return *this;
}
